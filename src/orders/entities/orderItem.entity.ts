import { Product } from "src/products/entities/product.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Order } from "./order.entity";


@Entity()
export class OrderItem {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    price: number

    @Column()
    qty: number

    @Column()
    total: number

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    // มองเป็นกลับกันว่ามันโยงไปที่ไหนแล้วมีได้กี่ตัว นึกถึงหน้าจารย์โกเมศ แล้วก็เขียนเลย
    // ลบ relation หลาย ไม่ได้ให้ลบด้วย onDelete: 'CASCADE'
    @ManyToOne(() => Order, (order) => order.orderItems, { onDelete: 'CASCADE' })
    order: Order;

    @ManyToOne(() => Product, (product) => product.orderItems, { onDelete: 'CASCADE' })
    product: Product;


}