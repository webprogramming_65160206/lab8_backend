import { InjectRepository } from "@nestjs/typeorm"
import { OrderItem } from "src/orders/entities/orderItem.entity"
import { Type } from "src/types/entities/type.entity"
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany, Repository } from "typeorm"

@Entity()
export class Product {
    constructor(@InjectRepository(Product) private usersRepository: Repository<Product>,) {

    }

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    price: number

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    // มันจะเชื่อมโยง อย่างแรกดูว่าเป็น many to one or one to many แล้วอีกฝั้งจะตามมาเอง อย่าลืมtype
    @ManyToOne(() => Type, (type) => type.products)
    type: Type;

    //ถ้าเป็น one to many มี attribute ต้องมี s
    @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
    orderItems: OrderItem[];

}
