import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './entities/role.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RolesService {
  constructor(@InjectRepository(Role) private rolesRepository: Repository<Role>,) {

  }
  create(createRoleDto: CreateRoleDto) {
    return this.rolesRepository.create(createRoleDto);
  }

  findAll() {
    return this.rolesRepository.find();
  }

  findOne(id: number) {
    return this.rolesRepository.findOneByOrFail({ id });
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {

    await this.rolesRepository.update(id, updateRoleDto)
    const updateRole = await this.rolesRepository.findOneByOrFail({ id })
    return updateRole;
  }

  async remove(id: number) {
    const removeRole = await this.rolesRepository.findOneByOrFail({ id })
    await this.rolesRepository.remove(removeRole)
    return removeRole;
  }
}
